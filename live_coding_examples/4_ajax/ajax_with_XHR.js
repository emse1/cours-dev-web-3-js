
/* 
    This function generate the DOM nodes to display a new comment,
    it takes as first parameter the text content of the comment to display
   
*/
function addNewCommentNode(commentText) {
  var commentsContainerNode = document.querySelector('.comments');

  var newCommentNode = document.createElement('p');
  newCommentNode.textContent = commentText;
  commentsContainerNode.appendChild(newCommentNode);  
  commentsContainerNode.appendChild(document.createElement('hr'));  
}


// Loading the list of comments when click on the button "Load comments"
/*
  1. Setup a event handler on click on the button
  2. Build a XHR request object, to do a GET request on http://localhost:3014/articles/1/comments,
     and setup the onload function that will be run on a successful response
  3. In the onload function, get the JSON data, parse it into a usable JS object with JSON.parse(),
     and with this data, generate new DOM elements to display the comments (we use the function
     addNewCommentNode() defined earlier in this file)
  4. We send the AJAX request with .send()
*/
document.querySelector('#load-comments-button').addEventListener('click', function() {

  var requestObject = new XMLHttpRequest();
  requestObject.open('GET', 'http://localhost:3014/articles/1/comments');
  requestObject.onload = function() {
    console.log('We received a response to our request');
    console.log(requestObject.response);

    // Now we want to generate DOM Node to display every comment we received as JSON
    var comments = JSON.parse(requestObject.response);

    comments.forEach(function(comment) {
      addNewCommentNode(comment.content)
    });
  }
  requestObject.send();
});


// How to make POST request

//
// We want to create a new comment when the user fills the "new comment" text area and clicks the "new comment" button
//
//
// 1. Set a handler on the click event of the button
// 2. On click, check that the textarea has content
// 3. Build a XHR request object, with method POST, to the endpoint for creating comments, set the body of the request
//    with a JSON string representing the comment object (see the endpoint documentation to see what JSON format is expected)
// 4. Send the request, on successful response, add the new comment to our HTML document
document.querySelector('#new-comment-publish-button').addEventListener('click', function() {
  var newCommentText = document.querySelector('#new-comment-textarea').value;

  if (!newCommentText) {
    // Textarea is empty, do nothing
    return;
  }

  var requestObject = new XMLHttpRequest();
  requestObject.open('POST', 'http://localhost:3014/articles/1/comments');
  
  requestObject.setRequestHeader("Content-Type", "application/json");

  requestObject.onload = function() {
    console.log('We received a response to our POST request');
    console.log(requestObject.response);

    // We add the comment to the document
    addNewCommentNode(newCommentText);
  }
  
  var newCommentObject = {
    author: "anonymous",
    content: newCommentText
  };

  requestObject.send(JSON.stringify(newCommentObject));
});




// To go further 
// Make the "send" button look disabled when the textarea is empty.
// For this we need to dynamically add the HTML attribute "disabled" to <button>, we need to write JS
// code that listen to textarea "change" event, every time something is written in the textarea, if 
// the new value is empty, we set the "disabled" attribute on <button>, otherwise we remove it


