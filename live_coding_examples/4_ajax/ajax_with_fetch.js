
/* 
    This function generate the DOM nodes to display a new comment,
    it takes as first parameter the text content of the comment to display
   
*/
function addNewCommentNode(commentText) {
  var commentsContainerNode = document.querySelector('.comments');

  var newCommentNode = document.createElement('p');
  newCommentNode.textContent = commentText;
  commentsContainerNode.appendChild(newCommentNode);  
  commentsContainerNode.appendChild(document.createElement('hr'));  
}




document.querySelector('#load-comments-button').addEventListener('click', function() {

  fetch('http://localhost:3014/articles/1/comments')
  .then(response => response.json()) // This line is necessary due to the fetch API using the "Promises" design
  .then((comments) => {
    console.log('We received a response to our request');
    console.log(comments);

    comments.forEach((comment) => {
      addNewCommentNode(comment.content);
    });
  });
});

// The fetch API has many powerfull options, see documentation for more details
// MDN : https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
// A cheat sheet : https://devhints.io/js-fetch




// How to make POST request

//
// We want to create a new comment when the user fills the "new comment" text area and clicks the "new comment" button
//
//
// 1. Set a handler on the click event of the button
// 2. On click, check that the textarea has content
// 3. Build a XHR request object, with method POST, to the endpoint for creating comments, set the body of the request
//    with a JSON string representing the comment object (see the endpoint documentation to see what JSON format is expected)
// 4. Send the request, on successful response, add the new comment to our HTML document
document.querySelector('#new-comment-publish-button').addEventListener('click', function() {
  var newCommentText = document.querySelector('#new-comment-textarea').value;

  if (!newCommentText) {
    // Textarea is empty, do nothing
    return;
  }

  fetch('http://localhost:3014/articles/1/comments', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      content: newCommentText,
      author: "anonymous"
    })
  })
  .then(response => response.json())
  .then(newComment => {
    addNewCommentNode(newComment.content);
  });

  
});


// To go further 
// Make the "send" button look disabled when the textarea is empty.
// For this we need to dynamically add the HTML attribute "disabled" to <button>, we need to write JS
// code that listen to textarea "change" event, every time something is written in the textarea, if 
// the new value is empty, we set the "disabled" attribute on <button>, otherwise we remove it
