# Frontend Web Development

## Introduction to Javascript

Quentin Richaud

qrichaud.pro@gmail.com

---

# What is a web application


![](./imgs/basic_webapp1.png)

---

# What is a web application : The Server Side

- The job of coding the server is called "backend development"
- The logic on the server can be more or less complex :
    - Simply serving static files
    - Dynamically generating HTML files
    - Using a database
    - Relying on other distributed services
    - etc.

---

# What is a web application : The Server Side

- The server is just a computer, it can run any compiled or interpreted language. Backend development
  can be done with a lot of languages and frameworks.
- Popular languages for backend development : 
    - PHP, Java, Python, Ruby, C#, Javascript (confusing, I know)

- Popular frameworks :
    - Laravel, Symfony (PHP)
    - Spring (Java)
    - Django (Python)
    - Ruby on Rails (Ruby)
    - ASP.NET (C#)
    - Express.js (Javascript)


---

# What is a web application : The Client Side

The client is your web browser (Firefox, Chrome, Edge, Safari, …).

It displays the HTML pages returned by the server.

Once the page is laoded, everything that happens in your browser is the client side logic.

Programming the things that run in the browser (on the client side), is what we call **frontend development**.

---

# Frontend

Frontend logic can be more or less complex.

Most basic : static pages (once loaded in the client).


Example : <https://css-tricks.com/should-a-website-work-without-javascript/>

---

# Basic frontend

![](./imgs/basic_webapp2.png)

---

# Frontend

Sites with a little bit of logic running in the browser : 

- hiding/showing elements
- making a message window appear
- opening/closing a menu
- reacting to events (clicks, focus, mouse move, keyboard keys pressed, …) 
- load more content without relaoding the page

We need something more than just HTML and CSS : Javascript

Example : <https://css-tricks.com/should-a-website-work-without-javascript/>

The button "Show All Comments" needs Javascript to work.

---

A little bit of frontend logic

![](./imgs/basic_webapp3.png)

---

# Frontend

Sites with a lot of logic in the browser : 

## Single Page Application 

They can look like standard websites, but once the page is loaded in the browser, it never reloads. All the navigation is made
by the Javascript code.

They allow to reuse the same server logic than a matching mobile application.

Example : Facebook, Twitter, …

## Web Applications

Websites that behave like a software. Example : Google Doc, Trello, Slack, …

---
![](./imgs/spa.png)

---

![](./imgs/mobile_app.png)

---

# Presenting : Javascript

- It’s the only language web browsers understand and can run
- Unlike backend development : frontend development can't choose its language, it's Javascript *


\* Maybe in the futur, Web Assembly is going to allow us to bypass Javascript, but it's not the case for now

---

# Javascript

Like with HTML and CSS : we need every browser to agree on the standard of Javascript.

We want our code to run the same on any user machine. Even if the latest browser (Firefox for example) ships
with an amazing new Javascript feature, we can't rely on it if we want all our userbase to be able to use our application.

---

# Javascript History

- 1995: At Netscape, Brendan Eich created "JavaScript".
- 1996: Microsoft releases "JScript", a port for IE3.
- 1997: JavaScript was standardized in the "ECMAScript" spec.
- 2005: "AJAX" was coined and the web 2.0 age begins.
- 2006: jQuery 1.0 was released.
- 2010: Node.JS was released.
- 2015: ECMAScript 6 was released.

Javascript has **nothing** in common with Java. The name is a marketing stunt. 

---

# Javascript problems

- First version was developped in a very short period of time
- Suffered from the war of browsers in the 2000s' (many inconsistensies between versions of the language depending on the browser).
- Because we need to rely on the rate of adoption of new browsers by the users : hard to make quick improvments on the language 
- Despite the latest versions being modern and fixing many early versions flaws : many legacy code to deal with, and it is hard
  to change the habits of programmers


---

# Javascript today


The language is specificated by the ECMA organization. The official name for the standard version is ECMAScript.

## ECMAScript5 (or ES5)

The specification of javascript published in **2009** by ECMA. 

That’s the most supported version of Javascript in today browsers. It is estimated that 99.44% of users use a browser that can run ES5.

It lacks a lot of modern language features, it is not ideal to write programs in ES5. Especially, it doesn't have a module (import, export, namespace isolation) system.

---

## ECMAScript6 (or ES6).

Also named **ECMAScript2015**. The specification of the language published in 2015 by ECMA.

Its adoption is estimated at 97.66%.

ES6 adds a lot of features compared to ES5, and that is very important from a software engineering point of view. 
It allows to make cleaner and more solid code. We will see that later.

Although the high adoption rate of supporting browsers, old habits die hard, and most companie still required their applications
to be shipped with ES5 code only. (It was necessary up to 3 years ago, ES6 adoption was < 95%, but not so much today).

---

# Javascript today

Although originally designed to be a scripting language for the browser, Javascript can now be run natively on computer 
with Node.JS. 

This allow to make backend development in Javascript (and use it as a general purpose scripting language, like Python or Ruby, …)

Not the topic of this lecture however.

---

# How to run Javascript in my web page


Anywhere in your webpage, add a script tag : 

```html
<html>
  <head></head>
  <body>
    <!-- html content -->
    <script src="./main.js"></script>
  </body>
</html>
```
    

It is recommended to put the script tags in the end of the `<body>` tag.

All scripts will run sequentially.

**See example 1**

---


You can also write javascript inline in the HTML file 



```html
<html>
  <head></head>
  <body>
    <!-- html content -->
    <script>
      console.log('something');
    </script>
  </body>
</html>
```

---

# How to inspect and debug my code

Chrome developer tools (Firefox developer tools also exists).

CTRL+Maj+I (Mac : Command+Option+I)


---

# Basics of JS syntax

See live coding See example 2.

Resources : 

- Cheat sheet with many JS syntax reminders : <https://htmlcheatsheet.com/js/>
- An efficient introduction to JS : <https://www.codecademy.com/learn/introduction-to-javascript/modules/learn-javascript-introduction/cheatsheet>


---

## Printing on the browser console

```js
console.log('Hello');
```

---

## Variables are weakly typed

```js
// Javascript in weakly typed
var myVariable;

myVariable = 3;
myVariable = "Some string";
```

```js
// ES5 Syntax
var myVar1; 

// ES6 Syntax
let myVar2; 
const myConstant;
```

---

## Basic types 

```js
// Numbers (no difference between integers and floats)
myVariable = 3;
myVariable = 5.0;

// Strings (can be simple quoted, double quoted, or backquoted)
myVariable = "A string";
myVariable = 'A string';
myVariable = `A string`; // ES6 only
// Backquoted strings allow to easily insert data in the string 
// (they are called string templates)
nameVariable = "Quentin";
myVariable = `Your name is ${nameVariable}`; // Will result in "Your name is Quentin"

// Arrays (types can be mixed in an array)
myArray = [1, 2, 3];
myArray = ['one', 2, 3];

// Booleans
myVariable = true;
myVariable = false;

// Objects
myObject = {
  property1: 'abc',
  prop2: 123
};
```

---

## Manipulating objects



```js

// Objects
// Unlike statically typed OO language, a JS object works like a dynamic map 
// with properties and values that can be added, removed, accessed on the fly. 
var firstObject = {
  propertyA: 2,
  probertyB: {
    subProperty: 'hi'
  }
};

// We can access object properties with dot notation or brackets
firstObject.propertyA;
firstObject['propertyA'];

// I can add other properties on an existing object
firstObject.propertyC = 'abc';
```

---

## Functions in JS

Javascript is a language that encourages functionnal programming.

Functionnal programming is a programming paradigm in which the programmer will solve algorithmical problems mainly by calling functions, instead of making imperative structures such as loops, conditionnal and jumps.

In order to do so, functions are treated as First Class Citizen. That mean, they are entity that can be manipulated like the other data types. You can assign a function to a variable, you can pass it as parameter to another function, and it can be a return value of a function. It can also be assigned to object properties.

---

## Functions in JS


```js
// Functions
// One key feature of JS, is that function are first class citizens, 
// they can be manipulated as other primitive types (objects, strings, …)

// This is a named function
function myFunctionA() {
  console.log('in myFunctionA()');
}

// Calling the function
myFunctionA();

// I can assign it to a variable, and call it through that variable
let myFunctionB = myFunctionA;
myFunctionB(); // Will print "in myFunctionA()"

// This is an anonymous function that is directly assigned to a variable
var myFunctionC = function() {
  console.log('in myFunctionC()');
}

myFunctionC(); // Will print "in myFunctionC()"
```
---

Functions are often passed as parameters to other functions. Such parameters are commonly designated as "callbacks", because the called function will "call back" the function given as a parameter.

```js

// Functions can be passed as arguments to other functions
function runCallbackAfterGreeting(callback) {
  console.log("Hello!");
  callback();
}


function enterUsername() {
  console.log("Can you enter your username?");
}

// passing a function to needsACallback()
runCallbackAfterGreeting(unterUsername);

// passing an anonymous function as parameter
runCallbackAfterGreeting(function() {
  console.log('Can you enter your password?');
});
```

---

## ES6 syntax for anonymous functions

```js
// ES6 syntax, arrow functions
let printAddition = (number1, number2) => {
  console.log(number1 + number2);
  return number1 + number2;
})

// Allow for shorter callback definition
runCallbackAfterGreeting(() => {
  console.log('Can you enter your username?');
});

// No need for parenthesis if only one argument
let greetName = name => {
  console.log(name)
}

// No need for brackets if only one instruction
let greetName = name => console.log(name);
```

---

## Functions in objects

```js
// On objects, we can emulate "methods" with properties holding functions
var person = {
  name: 'John',
  sayName: function () {
    console.log('My name is ' + this.name);
  }
}

person.sayName();
```

Beware, the behavior of `this` keyword in javascript is a bit complicated, 
if you are interested on the topic you can read more about it online, 
but it goes beyond the scope of this course.

The behavior is not the same if `this` is used inside a function defined with 
the ES5 syntax `function() {}` or the ES6 arrow syntax `() => {}`

---

## Functionnal programming in JS

Example with looping over an array


```js
// Imperative paradigm (the one you use the more often when learning programming)
var names = ['Alice', 'Bob', 'Charlie'];
for(var i = 0; i < names.length; i++) {
  var name = names[i];
  console.log(name.toUpperCase());
}
```

We will prefere to use a functionnal paradigm, permitted by the method `.forEach(callback)` of the Array data type. See : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach

```js
var names = ['Alice', 'Bob', 'Charlie'];
names.forEach(function(name) {
  console.log(name.toUpperCase());
})

// Or with ES6 function syntax
let names = ['Alice', 'Bob', 'Charlie'];
names.forEach(name => console.log(name.Uppercase()));
```

---

Example : mapping an array.

I want to make an array with all names in uppercase.

```js
// With imperative programming
var names = ['Alice', 'Bob', 'Charlie'];
var uppercaseNames = [];
for(var i = 0; i < names.length; i++) {
  uppercaseNames.push(names[i].toUpperCase());
}
```

```js
// With functional programming
let names = ['Alice', 'Bob', 'Charlie'];
let upperCaseNames = names.map(name => name.toUpperCase());
```

`.map()` method : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map

---

## Asynchronicity in JS

Due to the nature of the applications of javascript (programming behavior in web pages, the main task are waiting and reacting to user interaction with the interface, and networking, that is sending requests and waiting for responses), we have to deal with a lot of asynchronicity.

Javascript runtime as a particularity, that it is single threaded. You cannot make several concurrent threads like you would in other languages. There is always a single thead running and if you wait somewhere in your code, the whole application will freeze.

Let's consider the use case : we want to execute one action after a delay of 1s.

The following pseudo code is not possible in JS

```js
sleep(1000);
console.log('Hello!');
```

---

## Asynchronicity in JS

Asynchronous way to wait for a set amout of time, before doing a action, thanks to the functional paradigm : by using the `setTimeout()` native function : https://developer.mozilla.org/en-US/docs/Web/API/Window/setTimeout

```js
// Display "Hello" after 3000ms
function sayHello() {
  console.log("Hello");
}

console.log("Script has started");
setTimeout(sayHello, 3000);
```

Or the shorter version with an anonymous function in ES6 syntax 

```js
console.log("Script has started");
setTimeout(() => {
  console.log("Hello");
}, 3000)
```

Even shorter, with a one-liner function definition 

```js
setTimeout(() => console.log("Hello"), 3000);
```

---

## Asynchronicity : waiting for a network request

For this example, I use a fictional function (that may have been defined elsewhere in my project) : `getUserData()`.

This function makes a network request, and should retrieve the user data in the form of a JS 
object that looks likes this : 

```js
{
  id: 5,
  name: "Bob"
}
```

A network request is by nature asynchronous, so we need to use asynchronicity features 
and functionnal programming in order to wait for the result, and run subsequent instructions
after the result has been received.

---

## Asynchronicity : waiting for a network request


I can describe the function signature as follow : 

`getUserData(callbackFn)` : makes a request network to get the user data and calls the provided callback function with the results when the request is successfull.

- `callbackFn` : the callback function that is called with the request result given as parameter, when the request is successfull. The function is called with the following parameters :

  - `userObject` : an object that contains the user data

---

## Asynchronicity : waiting for a network request

This is how I would use it in my code.

Long version for pedagogical purposes : 

```js
// I want to request a user, and then display its name on the console

// 1. Preparing my callback function
function printUserData(userObject) {
  console.log(userObject.name);
}

getUserData(printUserData);
```

Short version, as I would write it in real life scenario :

```js
getUserData((userObject) => {
  console.log(userObject.name)
})
```

Even shorter syntax : 

```js
getUserData(userObject => console.log(userObject.name))
```

---

## Chaining asynchronous actions

Imagine that you have a series of asynchronous actions to perform, and each subsequent 
action is dependant on the result of the previous one. So you need to chain each action,
and deal with their asynchronous nature.

Scenario : I need to request the vendor that matches a given name, then I need to get its last customer,
then I need to get the list of orders placed by that customers. That is 3 network requests
in a row. We use these fictional functions : 

- `searchVendor(name, callbackFn)`. `callbackFn` is called with a parameter `vendorObject` that looks like `{id: 1, name: "myVendor"}`
- `getVendorUserList(vendorId, callbackFn)`. `callbackFn` is called with a parameter `userList` that looks like `[{id: 1, name: "User1"}, ...]`.
- `getUserOrders(userId, callbackFn)`. `callbackFn` is called with a parameter `userOrders`
that looks like `[{id: 1, title: "order 1"}, ...]`.

---

## Chaining asynchronous actions

Realisation of this scenario : 

```js
// Get the vendor from name
searchVendor("Manufrance", (vendorObject) => {
  // In my callback, when I received asynchronously the vendor Object
  // I can now make the next request
  getVendorUserList(vendorObject.id, (userList) => {
    // In the second callback, when I received the user list
    // I get the last user
    let lastUser = userList[userList.length - 1];
    // I now make the next request
    getUserOrders(lastUser.id, (orderList) => {
      // In the third callback, when I received the user order list
      console.log(`The user ${lastUser.name} has made ${orderList.length} order(s)`);
    })
  })
})
```

This is a valid way to program. However, we see that for each asynchronous action
that needs to be chained, we need to nest another level of callback, and soon become
entangled in what is colloquially called "callback hell".

---

## Several possible callbacks

We also need to react on failure of our request. This imaginary function provides 
a common way to do that, by giving the possibility to provide 2 callback functions, one
for each scenario (success, or failure).

`getUserData(callbackFn, errorCallbackFn)` : makes a request network to get the user data and calls the provided callback function `callbackFn` with the results when the request is successfull. If the request failed, then `errorCallbackFn` is called, with an object representing the error as first parameter.

`callbackFn` : the callback function that is called with the request result given as parameter, when the request is successfull. The function is called with the following parameters

  - `userObject` : an object that contains the user data

`errorCallbackFn` : the callback function that is called if the request failed. The function is called with the following parameters : 

  - `errorObject` : an object with the following format : `{errorCode: 500, errorMessage: "Internal Server Error"}`


---

## Several possible callbacks


Example of usage :

```js
getUserData((userData) => {
  console.log(`My user is ${userData.name}`);
}, (errorObject) => {
  console.log("An unexpected error occured");
  console.log(errorObject.message);
});
```

---

## The `Promise` object as workaround "callback hell"

To circumvent the so called "callback hell" problem, JS specification introduced a standardized
`Promise` object. Such an object represent a return data, that will be resolved asynchronously.

Instead of taking a callback, an asynchronous function will return a Promise object, and that 
object defines a `.then(callback)` function in order to asynchronously get the return value.

Promise object definition : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise

---

## The `Promise` object 
Let's redefine my `getUserData()` function so that is uses Promise instead of direct callback :

`getUserData(): Promise<UserObject>`. When called, this function makes a network request
to get user data, and then returns a Promise object that will resolve to the user data
once the request is successfull.

---

## The `Promise` object

Example : 

```js
let userDataPromise = getUserData();  // This function is run synchronously


// On the Promise object, I do an asynchronous action, with 
// the callback mecanism as seen before
userDataPromise.then((userData) => {
  console.log(`My user is ${userData.name}`);
})
```

Shorter use, without storing the Promise object in a named variable, but instead
directly using it from the return value :

```js
getUserData()
.then((userData) => {
  console.log(`My user is ${userData.name}`)
});
```

Even shorter : 

```js
getUserData().then(userData => console.log(`My user is ${userData.name}`));
```

---

## Chaining promises

So far, Promise object doesn't look like an improvement. Its value reside in its ability
to easily chain Promise. The `then()` method of a Promise object will itself return another
Promise object, which resolve to the result value callback function. 
If the resolve value of a Promise is itself a Promise, then it is internally "flattened", 
and in its resolve function you get the final value, not the nested Promise. 
So if in your `then()` method you need to perform another asynchronous action, which
will give you a Promise object, you can pass it as a return value of your callback function,
and then chain anothe `.then()` call after the first one.

Example, with again our chain of asynchronous actions. This time, `searchVendor(vendorName)`, 
`getVendorUserList(vendorId)`, and `getUserOrders(userId)` are defined without callback, 
but instead each of this function returns a Promise object, that will resolve to 
the expected data object.

---

## Chaining promises

Let's begin with the **wrong** way to use Promise chaining : 

```js
searchVendor("Manufrance")
.then((vendor) => {
  getVendorUserList(vendor.id)
  .then((userList) => {
    let lastUser = userList[userList.length - 1];
    getUserOrders(lastUser.id)
    .then((orderList) => {
      console.log(`The user ${lastUser.name} has made ${orderList.length} order(s)`);
    })
  })
})
```
 
In this bad example, we still a stuck in "callback hell".


---

## Chaining promises

Now each time we get a Promise
object inside a `then()` function, let's return it in order to flatten the chain.

```js
searchVendor("Manufrance")
.then((vendor) => {
  return getVendorUserList(vendor.id)
})
.then((userList) => {
  let lastUser = userList[userList.length - 1];
  return getUserOrders(lastUser.id)
})
.then((orderList) => {
  console.log(`The user has made ${orderList.length} order(s)`);
})
```

And now our process is flattened,and much more readable. Add short function syntax
each time is it possible, you can something even more readable.

```js
searchVendor("Manufrance")
.then(vendor => getVendorUserList(vendor.id))
.then(userList => getUserOrders(userList[userList.length - 1].id))
.then(orderList => console.log(`The user has made ${orderList.length} order(s)`));

// Note that for one-liner ES6 functions without brackets, 
// the "return" statement is implicit
```

---

## Making my own Promises in JS

Making your own promise is seldom. You will more likely use 
functions (native or from libraries) that give you Promise objects,
rather than doing your own.

However, I find that making at least once a Promise object yourself helps 
understand how it works.

We will make a function called `getUserAfter3s()` which will return a user object
after waiting 3 seconds. In fact, it will returns a Promise object, that Promise
object will resolve to the user object after 3s. Internally we will use the
native `setTimeout()` function, that doesn't use the Promise object.


---

## Making my own Promises in JS


```js
function getUserAfter3s() {
  let resultUser = {id: 1, name: "Bob"};

  let resultPromise = new Promise((resolve) => {
    // We use the "object constructor" syntax, that I didn't describe, but it
    // works like in other OO languages.

    // The constructor takes a function as parameter. This function is run 
    // immediatly, so this is where you put the main logic of your asynchronous 
    // action.

    // This function is given a first parameter "resolve", which is the callback
    // that will be provided to the `.then()` method of your callback object. 
    // So when your asynchronous action is ready, call the `resolve` function and
    // give it as first parameter the value you want your Promise to resolve to.

    
    setTimeout(() => {
      resolve(resultUser);
    }, 3000);
  });

  return resultPromise;
}
```


---

## Making my own Promises in JS


And this is how I use this function 

```js
getUserAfter3s()
.then(userObject => console.log(userObject.name));
```

---

## The await/async syntax

Since Promise objects became a standard way to deal with asynchronicity, JS introduce
a new syntax to make Promise manipulation more easy, with the `await` and `async`
keywords. In fact, these keywords hide the Promise objects, although they are still used
under the hood.

You can declare a function as asynchronous using the `async` keyword.

```js
function getUser1() {
  // This is a synchronous function
}

async function getUser2() {
  // This is an asynchronous functios
}

let getUser3 = async () => {
  // Also works with arrow functions
}
```

---

## The await/async syntax

An asynchronous function will **always** return a Promise. This is made automatically, 
the return value of an `async` function is wrapped in a Promise by the JS engine.

```js 
async function getUser() {
  return {name: "Bob"};
}

// is the same than
function getUser() {
  return new Promise((resolve) => {
    resolve({name: "Bob"});
  })
}
```

---

## The await/async syntax

If your are **inside** an `async` function, you now have access to the `await` 
keyword. The `await` keyword must be used in front of a Promise object (or 
an `async` function, since `async` funtions **always** return Promises)
and allow you to access the resolve value without using the `.then()` method and a callback.

```js
async function printUser() {
  let user = await getUser();
  console.log(`My user is ${user}`);
}

// is the same than 
function printUser() {
  getUser()
  .then((user) => {
    console.log(`My user is ${user}`);
  })
}
```

This `await` keyword simulates a pause in the JS code, although under the hood it is 
not the case, the JS engine transform it as a callback mecanism. That's why the `await`
keyword only available in a `async` function, so that the asynchronicity can be wrapped
in the inevitable Promise object the function will return, in order to get that
pseudo synchronous syntax.

---


# Advanced JS syntax and principles

These concepts are not necessary for basic JS developments, however for those wo want to go further : 


- JS is a Prototype Based Oriented Object language (unlike pure OO languages like Java). 
    - <https://en.wikipedia.org/wiki/Prototype-based_programming>
    - <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain>
- JS runs in a single threaded environment, the code is non blocking
- The difference in scope behavior of variables, when declaring them with `var` vs `let`
- How `this` keyword works. Especially, difference in behavior when functions are defined with `function()` and with `() =>`.
- JS is designed to deal with asynchronicity a lot, by the use of callback functions, Promises,
  and the `async`/`await` syntax (which is syntaxic sugar around Promises).
- Understand how closures work in JS : <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures>

---

# Manipulating the DOM with JS

The DOM (Document Object Model). It’s the programmatic way to represent and access the elements 
of my HTML page (every HTML tag `<div>`, `<p>`, … makes a DOM node).

![](./imgs/domtree.png)

Each element of the tree is a DOM node.

---

# Manipulating the DOM with JS

The web API provided by the browser in the JS runtime environment, allows us to access the content of the webpage
as DOM elements.

Operations we can do :

- Access one ore multiple DOM nodes
- Traverse the DOM : from one node, find parents, children or sibling nodes
- Modify existing nodes, create and insert new nodes in the DOM tree, delete existing nodes
- React on events happening on DOM elements : clicks, focus, …

---

# Manipulating the DOM with JS

## Resources

- Cheat sheets : 
    - <https://gist.github.com/thegitfather/9c9f1a927cd57df14a59c268f118ce86>
    - <https://github.com/anish000kumar/Javascript-DOM-API-cheatsheet>
- Exhaustive Documentation (MDN) : <https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction>

See live coding example 3.

---

## Selecting a DOM Node

```js
// Get unique element by Id
let myDOMNode = document.getElementById('title');

// Get first element that matches the selector (same selector syntax than CSS selectors)
let commentsDiv = document.querySelector('.comments');

// Get a list of elements that match the selector
let commentDivs = document.querySelectorAll('.comment');
```

---

## Manipulating a DOM node

```js
var paragrapheNode = document.querySelctor('.main-paragraph');
paragrapheNode.textContent = "My new content";
paragrapheNode.classList.toggle('warning');

```

The webpage render will be updated in real time. This JS code can easily modify the style of the DOM node, if it is used in combination with a CSS stylesheet like this :

```css
.warning {
  background-color: red;
}
```

---

## Create new DOM nodes and insert them into the DOM tree

```js
// Create new node, not yet attached to the tree (won't render)
let newParagraph = document.createElement("p");

newParagraph.textContent = "I disagree with the previous comment. This was generated by JS code";

// Insert the node into the document
let commentsDiv = document.querySelector('.comments');
commentsDiv.appendChild(newParagraph);
```

---

## Use HTML templates to easily create nodes

```js
let templateToInsert = `<p>Another comment generated from JS</p> <hr/>`;
commentsDiv.insertAdjacentHTML('beforeend', templateToInsert);
```

---

## React to event happening on DOM nodes

```js
// React to events happening in  the document
let buttonNode = document.querySelector('#show-hide-button');


buttonNode.addEventListener('click', function(event) {
   console.log('Button was clicked!');
});
```


---

# AJAX

AJAX means “asynchronous JavaScript and XML\*”.

Using Javascript to retrieve additionnal data from the server after the page as loaded, in order to dynamically update the
content of the page without a page reload.

\* Historically, XML was the preferred format for sending raw data. Nowadays we use JSON predominantly.

---

# AJAX

![](./imgs/basic_webapp3.png)

---

# Setting up the backend server

See the README under `./backend_server`.

You can test the API in a terminal using `curl`. 

- Get the list of articles : `curl http://localhost:3014/articles`


---

# AJAX : How to do it in JS code

## The old school way

The browser web API provides an object : the `XMLHttpRequest` object (see MDN doc for all details). 
Ofter shortened as XHR.

<https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest>

Despite the name, you can send and receive any format of data, not just XML.

## The modern way

The browser web API provides the `fetch()` function. <https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API>

---

## AJAX with `XMLHttpRequest`

```js
  let requestObject = new XMLHttpRequest();
  requestObject.open('GET', 'http://localhost:3014/articles/1/comments');
  requestObject.onload = function() {
    // Process response
    console.log('We received a response to our request');
    console.log(requestObject.response);
  }
  requestObject.send();
```

---

## AJAX with `fetch()`

```js
fetch('http://localhost:3014/articles/1/comments')
.then((response) => {
  // Process response here
});
```

Or with `async`/`await` syntax :

```js
async function getComments() {
  let response = await fetch('http://localhost:3014/articles/1/comments');
  // Process response here

}

getComments();
```

---



## Using an external library

External libraries will use XMLHttpRequest  or fetch() under the hood. However they can give you a more flexible API
to make AJAX.

Some libraries for AJAX : jQuery (outdated, and does many other things than ajax), axios, SuperAgent.

---

# AJAX - Pro and cons

## XMLHttpRequest

- Pro : Oldest standard, most likely to be supported by browsers, and known by developpers.
- Cons : the API is hard to use, ugly code.

## fetch

- Pro : modern and easy to use API, natively supported by browsers (no library overhead)
- Cons : "only" supported by 96.37% of browsers on the market

---


## external library

- Pro : modern and easy to use API, will convert to `XMLHttpRequest` under the hood if browser does not support `fetch()`
- Cons : add overhead to native JS code (size of JS library adds weight to your website)

See live coding example 4.

---

# Modules in javascript

Almost all modern languages have a module system, in order to isolate variable naming scope between the different source files.

A variable `a` declared in a source file `a.source` won't be accessible and modifiable in another source file `b.source`.

The way to share variables (or objects, funntions, …) between source file is usually done trough an import/export mechanism.

For example in Java : 

```Java
// MyClass.java

public class MyClass {
}


!Java
// Main.java
import org.package.MyClass;
```

`MyClass` is accessible from the outside only if I explicitly declare it `public`. Outside code needs to explicitly import it.

---

# Modules in javascript

**ES5 has no module system**. Everything is global and the global namespace is shared between all scripts running in the page.

```html
<html>
  <head></head>

  <body>
    <script src="./scriptA.js"></script>
    <script src="./scriptB.js"></script>
  </body>
</html>
```


```js
// scriptA.js
var a = 'hello';
```

```js
// scriptB.js
a = 'changing my variable';
```

---

# Modules in javascript

## ES6 introduces native modules

```html
<html>
  <head></head>

  <body>
    <script src="./scriptB.js" type="module"></script>
  </body>
</html>
```

```js
// scriptA.js
var a = 'hello';
var b = 'world';

export {a};
```

```js
// scriptB.js
import {a} from "./scriptA.js";

a = 'changing my variable';
// won't affect 'a' in scriptA.js because a is a local copy of the exported variable
```


---

# Module in JS : going further

Before the introduction of ES6 modules, the module problem was solved by external JS libraries that would 
simulate the fonctionality of a module system (that's why we say ES6 are native modules, because other module
systems were non native).

Several systems were competing, we can still encounter them in existing codebase, althought ES6 modules tend to replace them.

Some of them : CommonJS, AMD, UMD.

The commonJS syntax is still the norm for node.js code (Node has been slow to implement support of ES6 modules).


---

# Module in JS : going further

## Example of CommonJS syntax

```js
// scriptA.js
var a = 'hello';

module.exports = {
  a: a
};
```

```js
// scriptB.js
var importedModule = require('./scriptA.js');
var a = importedModule.a; 
```

---

# A popular external library : JQuery

We saw how it is possible to make a lot of things with the Javascript and the native web APIs provided by all browsers.

Browsers supporting ES6 and moderns APIs such as `fetch()` allow us to write efficient code without relying on an external library.

In the old times on ES5, making things only with Javascript was a little bit tedious (see how long it is to build an XMLHttpRequest object for example).

There are a lot of JS libraries that provides tools to do common task a lot easiers. The most popular library from the end of 2000s’ and beginning of 2010s’ was JQuery.

Nowadays it is outdated, and not a lot of new projects use it. However it is still present in many existing applications, 
and had a huge influence in the Javascript community. So it’s important to have a look at it.

---

# Why JQuery in 2020 ?

It is important you know JQuery in 2020 because : 

- It had a huge influence in the past 15 years in web development
- You will still see it in a lot of legacy code base
- Many modern frameworks and libraries were influenced by it
- It helper shape the modern Javascript culture

---

# Why JQuery in 2020 ?

However :

- It is outdated : many necessary features it provided are now covered by ES6 and browsers native APIs
- It's a huge catch-all library. If you need a library to do a certain task (Ajax for example), you better find a specialized
  and smaller library aimed at solving your problem.
- If you have complex code to write, you should use modern frameworks such as Angular, React or VueJS (among others). Using only
  JQuery for complex features will result in spaghetti code.


I encourage you to know JQuery to improve your knowledge of web development history. I strongly discourage you to use JQuery in new projects. 

A frontend developper that doesn't know how to solve a problem without JQuery is not a good frontend developper.

<http://youmightnotneedjquery.com/>

---

# Using JQuery

Because JQuery is an old library, it doesn't use modern module system. 

To import it : include the libary source file in your HTML document before your script. It will create a global variable named "$" 
that has all the library functions as properties.

The official documentation : <https://api.jquery.com/>

A cheat sheet : <https://github.com/ga-wdi-lessons/js-jquery/blob/master/jquery-cheat-sheet.md>

See live demo 

---

# What next?

You have a great assignment on eCampus ! Link : <https://ecampus.emse.fr/mod/assign/view.php?id=29839>

---

# What next ?

You can play with the demo code provided in the lecture repository.

# How to setup the backend to do AJAX

Under `./backend_server`, check the README in order to launche the demo API server. You have an API which gets you articles and comments in JSON format.
You can also create new articles and comments with POST request (the API is documented in the README).

In order to run your application, you need to have 2 servers running :

- The frontend server, (the same that runs this presentation, launched by `.run.sh` at the root of the repository). It serves static files, 
for the frontend that is the HTML, CSS and JS files. It runs by default on `localhost:8000`.
- The API server, launched under the `./backend_server` directory (you need to follow the README instructions). 
It serves data through a JSON API. By default it runs on `localhost:3014`.

---

